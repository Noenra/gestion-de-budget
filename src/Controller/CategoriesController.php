<?php

namespace App\Controller;

use App\Entity\Categories;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController
{
    #[Route('/categories', name: 'app_categories')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $categories = $doctrine->getRepository(Categories::class)->findAll();
        foreach ($categories as $category) {
            $res[] = array(
                'id' => $category->getId(),
                'name' => $category->getCategorieName()
            );
        }
        return $this->json(['categories' => $res], Response::HTTP_OK);
    }
}
