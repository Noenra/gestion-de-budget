<?php

namespace App\Controller;

use App\Entity\Users;
use App\Entity\Transactions;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TransactionsController extends AbstractController
{
    #[Route('/transactions', name: 'app_transactions')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $transactions = $doctrine->getRepository(Transactions::class)->findAll();
        $users = $doctrine->getRepository(Users::class)->findBy(['transactions'=>getId());
        //dd($transactions);
        foreach ($transactions as $transactor) {
            $userid = $transactor->getUsersId();
            foreach($users as $user){
                dd($user->getId());
            }
            $res[] = array(
                'id' => $transactor->getId(),
                'calcul' => $transactor->getCalculInt(),
                'title' => $transactor->getTitle(),
                'user_id' => $transactor->getUsersId(),
                'categories_id' => $transactor->getCategoriesId()
            );
        }
        return $this->json([
            'calcul' => '',
            'path' => 'src/Controller/TransactionsController.php',
        ]);
    }
}
