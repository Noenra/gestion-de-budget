<?php

namespace App\Entity;

use App\Repository\TransactionsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransactionsRepository::class)]
class Transactions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $calculInt;

    #[ORM\Column(type: 'string', length: 45, nullable: true)]
    private $title;

    #[ORM\ManyToOne(targetEntity: Users::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $Users_id;

    #[ORM\ManyToOne(targetEntity: Categories::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $Categories_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalculInt(): ?int
    {
        return $this->calculInt;
    }

    public function setCalculInt(int $calculInt): self
    {
        $this->calculInt = $calculInt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUsersId(): ?Users
    {
        return $this->Users_id;
    }

    public function setUsersId(?Users $Users_id): self
    {
        $this->Users_id = $Users_id;

        return $this;
    }

    public function getCategoriesId(): ?Categories
    {
        return $this->Categories_id;
    }

    public function setCategoriesId(?Categories $Categories_id): self
    {
        $this->Categories_id = $Categories_id;

        return $this;
    }
}
